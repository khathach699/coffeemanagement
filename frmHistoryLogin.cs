﻿using DevExpress.XtraEditors.Design;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static DevExpress.Drawing.Printing.Internal.DXPageSizeInfo;
using static DevExpress.XtraEditors.Mask.Design.MaskSettingsForm.DesignInfo.MaskManagerInfo;
using iTextSharp.text.pdf;
using iTextSharp.text;
using testb1.context;

namespace testb1
{
    public partial class frmHistoryLogin : Form
    {
        private AccountModel context = new AccountModel();
        private List<LoginHistory> allHistories;

        public frmHistoryLogin()
        {
            InitializeComponent();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmMenu m = new frmMenu();
            this.Hide();
            m.ShowDialog();
        }

        private void HistoryLogin_Load(object sender, EventArgs e)
        {
            // Populate your ComboBox with Shifts
            var shiftWorkList = context.ShiftWorks.ToList();
            shiftWorkList.Insert(0, new ShiftWork { ShiftName = "" });
            comboBox1.DataSource = shiftWorkList;
            comboBox1.DisplayMember = "ShiftName";
            comboBox1.ValueMember = "ShiftID";

            // Load all histories
            allHistories = context.LoginHistories.ToList();
            BindGrid(allHistories);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterAndBindGrid();
         
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            // Filter and update DataGridView based on Date
            FilterAndBindGrid();
        }
        private void FilterAndBindGrid()
        {
            int selectedShiftID = 0; // Default value for no selection

            if (comboBox1.SelectedValue != null && int.TryParse(comboBox1.SelectedValue.ToString(), out selectedShiftID))
            {
                var selectedDate = dateTimePicker1.Value.Date;

                var filteredHistories = allHistories
                    .Where(history => (selectedShiftID == 0 || history.ShiftWork?.ShiftID == selectedShiftID) &&
                                      (selectedDate == DateTime.MinValue || history.Date.Date == selectedDate))
                    .ToList();

                BindGrid(filteredHistories);
            }

        }

        private void BindGrid(List<LoginHistory> histories)
        {
            dataGridView1.Rows.Clear();

            foreach (var item in histories)
            {
                int index = dataGridView1.Rows.Add(item);
                dataGridView1.Rows[index].Cells[0].Value = item.StaffID;
                dataGridView1.Rows[index].Cells[1].Value = item.Position?.PositionName;
                dataGridView1.Rows[index].Cells[2].Value = item.Date;
                dataGridView1.Rows[index].Cells[3].Value = item.ShiftWork?.ShiftName ?? string.Empty;


            }
            updateTotal();

        }
        private void updateTotal()
        {
            int row = dataGridView1.Rows.Count;
            textBox1.Text = row.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1 == null || dataGridView1.Columns.Count == 0 || dataGridView1.Rows.Count == 0)
                {
                    MessageBox.Show("Please make sure dataGridView1 is properly initialized and contains data.");
                    return;
                }

                Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel.Worksheet ws = null;
                ws = workbook.Worksheets["Sheet1"];
                ws = workbook.ActiveSheet;
                ws.Name = "HistoryDetail";

                for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
                {
                    ws.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
                }

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        object cellValue = dataGridView1.Rows[i].Cells[j].Value;
                        if (cellValue != null)
                        {
                            ws.Cells[i + 2, j + 1] = cellValue.ToString();
                        }
                        else
                        {
                            // Xử lý trường hợp giá trị là null (nếu cần)
                            // Ví dụ: ws.Cells[i + 2, j + 1] = "Giá trị null";
                        }
                    }
                }

                var save = new SaveFileDialog();
                save.FileName = "Untitled.xlsx";
                save.Filter = "XLSX (*.xlsx)|*.xlsx";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    ws.SaveAs(save.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    MessageBox.Show("Data Export Successfully", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                app.Quit();
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
                if (dataGridView1.Rows.Count > 0)
                {
                    SaveFileDialog save = new SaveFileDialog();
                    save.Filter = "PDF (*.pdf)|*.pdf";
                    save.FileName = "Untitled.pdf";
                    bool ErrorMessage = false;
                    if (save.ShowDialog() == DialogResult.OK)
                    {
                        if (File.Exists(save.FileName))
                        {
                            try
                            {
                                File.Delete(save.FileName);
                            }
                            catch (Exception ex)
                            {
                                ErrorMessage = true;
                                MessageBox.Show("Unable to wride data in disk" + ex.Message);
                            }
                        }
                        if (!ErrorMessage)
                        {
                            try
                            {
                                 PdfPTable pTable = new PdfPTable(dataGridView1.Columns.Count);
                                pTable.DefaultCell.Padding = 2;
                                pTable.WidthPercentage = 100;
                                pTable.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                                foreach (DataGridViewColumn col in dataGridView1.Columns)
                                {
                                    PdfPCell pCell = new PdfPCell(new Phrase(col.HeaderText));
                                    pTable.AddCell(pCell);
                                }
                            foreach (DataGridViewRow viewRow in dataGridView1.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    if (dcell.Value != null)
                                    {
                                        pTable.AddCell(dcell.Value.ToString());
                                    }
                                    else
                                    {
                                        pTable.AddCell(""); // hoặc xử lý khác tùy theo nhu cầu của bạn
                                    }
                                }
                            }
                            using (FileStream fileStream = new FileStream(save.FileName, FileMode.Create))
                                {
                                iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                    document.Open();
                                    document.Add(pTable);
                                    document.Close();
                                    fileStream.Close();
                                }
                                MessageBox.Show("Data Export Successfully", "Notification");
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Error while exporting Data" + ex.Message);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No Record Found", "Notification");
                }
            }
        }
    }


