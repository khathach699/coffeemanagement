﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using testb1.context;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;


namespace testb1
{
    public partial class frmAccout : Form
    {
        public frmAccout()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            BinGrid(context.Accounts.ToList());
            List<Position> offices = context.Positions.ToList();

            this.comboBox1.DataSource = offices;
            this.comboBox1.DisplayMember = "PositionName";
            this.comboBox1.ValueMember = "PositionID";
        }

        private void BinGrid(List<Account> accounts)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in accounts)
            {
                int index = dataGridView1.Rows.Add(item);
                dataGridView1.Rows[index].Cells[0].Value = item.StaffID;
                dataGridView1.Rows[index].Cells[1].Value = item.UserName;
                dataGridView1.Rows[index].Cells[2].Value = item.PassWord;
                dataGridView1.Rows[index].Cells[3].Value = item.Email;
                dataGridView1.Rows[index].Cells[5].Value = item.PhoneNumber;
                if (item.Position != null)
                {
                    dataGridView1.Rows[index].Cells[4].Value = item.Position.PositionName;
                }


            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {

            frmMenu m = new frmMenu();
            this.Hide();
            m.ShowDialog(); ;
        }


        private bool IsValidEmail(string email)
        {
            // Kiểm tra email theo biểu thức chính quy
            string pattern = @"^[A-Za-z0-9._%+-]+@gmail\.com$";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }

        private void btnAccountAdd_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells[2].Value != null && row.Cells[2].Value.ToString() == txtemail.Text || row.Cells[5].Value != null && row.Cells[5].Value.ToString() == txtPhoneNumber.Text)
                    {
                        MessageBox.Show("Email or Phone Number already exists, please check again");
                        return;
                    }
                }

                if (string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrEmpty(txtAcountName.Text)
                    || string.IsNullOrEmpty(txtemail.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
                }
                // Kiểm tra email theo biểu thức chính quy
                else if (!IsValidEmail(txtemail.Text))
                {
                    MessageBox.Show("Email không hợp lệ. Vui lòng nhập một email không dấu và kết thúc bằng '@gmail.com'.");
                }
                else if (txtPhoneNumber.TextLength != 10)
                {
                    MessageBox.Show("Phone number must have 10 characters", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Account b = new Account()
                    {
                        StaffID = txtStaffID.Text,
                        UserName = txtAcountName.Text,
                        PassWord = txtPassword.Text,
                        Email = txtemail.Text,
                        PhoneNumber = txtPhoneNumber.Text,
                        PositionID = (int)comboBox1.SelectedValue
                    };
                    AccountModel  context = new AccountModel();
                    context.Accounts.Add(b);
                    context.SaveChanges();
                    MessageBox.Show("Add successfulyy", "confirm", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BinGrid(context.Accounts.ToList()); ;
                    txtStaffID.Clear();
                    txtAcountName.Clear();
                    txtPassword.Clear();
                    txtPassword.Clear();
                    txtPhoneNumber.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAccountDetele_Click(object sender, EventArgs e)
        {
            try
            {
                AccountModel context = new AccountModel();
                Account account = context.Accounts.FirstOrDefault(p => p.StaffID == txtStaffID.Text);
                if (account != null)
                {
                    context.Accounts.Remove(account);
                    context.SaveChanges();
                    MessageBox.Show("deleted successfully", "confirm", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BinGrid(context.Accounts.ToList()); ;
                    txtStaffID.Clear();
                    txtAcountName.Clear();
                    txtPassword.Clear();
                    txtPassword.Clear();
                    txtPhoneNumber.Clear();

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAccountUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //foreach (DataGridViewRow row in dataGridView1.Rows)
                //{
                //    if (row.Cells[2].Value != null && row.Cells[2].Value.ToString() == txtEmai.Text)
                //    {
                //        MessageBox.Show("Email đã tồn tại, vui lòng nhập một email khác.");
                //        return;
                //    }
                //}

                if (string.IsNullOrEmpty(txtAcountName.Text) || string.IsNullOrEmpty(txtPassword.Text)
                    || string.IsNullOrEmpty(txtemail.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin.");
                }
                // Kiểm tra email theo biểu thức chính quy
                //else if (!IsValidEmail(txtEmai.Text))
                //{
                //    MessageBox.Show("Email không hợp lệ. Vui lòng nhập một email không dấu và kết thúc bằng '@gmail.com'.");
                //}
                else
                {
                    AccountModel context = new AccountModel();
                    Account account = context.Accounts.FirstOrDefault(p => p.Email == txtemail.Text);
                    if (account != null)
                    {
                        account.StaffID = txtStaffID.Text;
                        account.UserName = txtAcountName.Text;
                        account.PassWord = txtPassword.Text;
                        account.Email = txtemail.Text;
                        account.PhoneNumber = txtPhoneNumber.Text;
                        account.PositionID = (int)comboBox1.SelectedValue;

                        context.SaveChanges();
                        MessageBox.Show("Update successfully", "confirm", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BinGrid(context.Accounts.ToList()); ;
                        txtStaffID.Clear();
                        txtAcountName.Clear();
                        txtPassword.Clear();
                        txtPassword.Clear();
                        txtPhoneNumber.Clear();


                    }
                    else
                    {

                        MessageBox.Show("not found Email", "Confirm", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    txtStaffID.Text = row.Cells[0].Value.ToString();
                    txtAcountName.Text = row.Cells[1].Value.ToString();
                    txtPassword.Text = row.Cells[2].Value.ToString();
                    txtemail.Text = row.Cells[3].Value.ToString();
                    comboBox1.Text = row.Cells[4].Value.ToString();
                    txtPhoneNumber.Text = row.Cells[5].Value.ToString();

                }
            }
            catch
            {
                MessageBox.Show("error");
            }

        }
        

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            frmWorkShift s = new frmWorkShift();
            this.Hide();
            s.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmHistoryLogin h = new frmHistoryLogin();
            this.Hide();
            h.ShowDialog();
        }

        private void txtPhoneNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void FindAcount_TextChanged(object sender, EventArgs e)
        {
            string key = txtFindAcount.Text;
            AccountModel context = new AccountModel();
            if (string.IsNullOrEmpty(key))
            {
                BinGrid(context.Accounts.ToList());
            }
            else
            {
                var search = context.Accounts.Where(a => a.StaffID.Contains(key) ||
                                                        a.UserName.Contains(key) ||
                                                        a.Email.Contains(key) ||
                                                        a.PassWord.ToString().Contains(key) ||
                                                        a.PhoneNumber.ToString().Contains(key) ||
                                                        a.Position.PositionName.Contains(key)).ToList();


                BinGrid(search);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
