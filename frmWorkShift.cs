﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using testb1.context;

namespace testb1
{
    public partial class frmWorkShift : Form
    {

        public frmWorkShift()
        {
            InitializeComponent();
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "HH:mm";
            dateTimePicker1.ShowUpDown = true;
            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "HH:mm";
            dateTimePicker2.ShowUpDown = true;

        }

        private void label2_Click(object sender, System.EventArgs e)
        {

        }

        private void btnExit_Click(object sender, System.EventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            try
            {
                // Check if the txtShiftode and txtNameShift are valid numbers
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells[0].Value != null && row.Cells[0].Value.ToString() == txtShiftode.Text)
                    {
                        MessageBox.Show("ID Shift Work already exists, please enter a different Shift ID.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                ShiftWork b = new ShiftWork()
                {
                    ShiftID = int.Parse(txtShiftode.Text),
                    ShiftName = txtNameShift.Text,
                    StartTime = DateTime.Now.Date.Add(dateTimePicker1.Value.TimeOfDay),
                    EndTime = DateTime.Now.Date.Add(dateTimePicker2.Value.TimeOfDay)
                };

                AccountModel context = new AccountModel();
                context.ShiftWorks.Add(b);
                context.SaveChanges();
                MessageBox.Show("Added successfully", "Confirm", MessageBoxButtons.OK, MessageBoxIcon.Information);
                BinGrid(context.ShiftWorks.ToList());
                txtShiftode.Clear();
                txtNameShift.Clear();
                dateTimePicker1.Value = DateTime.Now; // Reset date picker values
                dateTimePicker2.Value = DateTime.Now; // Reset date picker values
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void ShiftManager_Load(object sender, System.EventArgs e)
        {
            AccountModel context = new AccountModel();
            BinGrid(context.ShiftWorks.ToList());

        }

        private void BinGrid(List<ShiftWork> accounts)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in accounts)
            {
                int index = dataGridView1.Rows.Add(item);
                dataGridView1.Rows[index].Cells[0].Value = item.ShiftID;
                dataGridView1.Rows[index].Cells[1].Value = item.ShiftName;
                dataGridView1.Rows[index].Cells[2].Value = item.StartTime.ToString("HH:mm");
                dataGridView1.Rows[index].Cells[3].Value = item.EndTime.ToString("HH:mm");
            }
        }

       

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // check if the txtStartShift and txtAndShift are valid numbers


                AccountModel context = new AccountModel();
                int shiftCode;
                if (int.TryParse(txtShiftode.Text, out shiftCode))
                {
                    ShiftWork b = context.ShiftWorks.FirstOrDefault(p => p.ShiftID == shiftCode);
                    if (b != null)
                    {
                        b.ShiftName = txtNameShift.Text;
                        b.StartTime = DateTime.Now.Date.Add(dateTimePicker1.Value.TimeOfDay);
                          b.EndTime = DateTime.Now.Date.Add(dateTimePicker2.Value.TimeOfDay);

                        context.SaveChanges();
                        MessageBox.Show("Updated Successfully", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BinGrid(context.ShiftWorks.ToList()); ;
                        txtShiftode.Clear();
                        dateTimePicker1.Value = DateTime.Now;
                        txtNameShift.Clear();
                        dateTimePicker2.Value = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("Not found Shift ID", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                else
                {
                    MessageBox.Show("Error", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            int shiftCode;
            if (int.TryParse(txtShiftode.Text, out shiftCode))
            {
                ShiftWork b = context.ShiftWorks.FirstOrDefault(p => p.ShiftID == shiftCode);
                if (b != null)
                {
                    context.ShiftWorks.Remove(b);
                    context.SaveChanges();
                    MessageBox.Show("Deleted Successfully", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BinGrid(context.ShiftWorks.ToList()); ;
                    txtShiftode.Clear();
                    dateTimePicker1.Value = DateTime.Now;
                    txtNameShift.Clear();
                    dateTimePicker2.Value = DateTime.Now;
                }
                else
                {
                    MessageBox.Show("Not found Shift ID", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Error", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    txtShiftode.Text = row.Cells[0].Value.ToString();
                    txtNameShift.Text = row.Cells[1].Value.ToString();

                    if (DateTime.TryParse(row.Cells[2].Value.ToString(), out DateTime startTime))
                    {
                        dateTimePicker1.Value = startTime; 
                    }

                    if (DateTime.TryParse(row.Cells[3].Value.ToString(), out DateTime endTime))
                    {
                        dateTimePicker2.Value = endTime; 
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void dateTimePicker1_ValueChanged_1(object sender, EventArgs e)
        {
            DateTime selectedTime = dateTimePicker1.Value;
            DateTime selectedTime2 = dateTimePicker2.Value;
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmMenu m = new frmMenu();
            this.Hide();
            m.ShowDialog();
        }
    }
}

