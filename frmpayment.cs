﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace testb1
{
    public partial class frmpayment : Form
    {
     
        public frmpayment()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private string GetItemName(int itemID)
        {
            // Kết nối đến cơ sở dữ liệu
            using (AccountModel context = new AccountModel())
            {
                // Truy vấn để lấy tên sản phẩm dựa trên ItemID
                var item = context.Items.FirstOrDefault(i => i.ItemID == itemID);

                // Kiểm tra xem có sản phẩm nào tương ứng không
                if (item != null)
                {
                    // Trả về tên sản phẩm
                    return item.ItemName;
                }
            }

            // Nếu không tìm thấy sản phẩm tương ứng, trả về một giá trị mặc định hoặc thông báo lỗi
            return "Product name does not exist";// hoặc return null;
        }

        private void UpdateTextbox2()
        {
            textBox2.Text = SharedData.SharedTotal.ToString();

            // Hiển thị số lượng sản phẩm từ FormMenu
            //textBoxItemQuantity.Text = frmMenu.ItemCount.ToString();

            foreach (var item in frmMenu.OrderItems)
            {
                int index = dgvOrder.Rows.Add();
                int itemID = item.ItemID;
                string itemName = GetItemName(itemID);
                dgvOrder.Rows[index].Cells[0].Value = itemName;
                dgvOrder.Rows[index].Cells[1].Value = item.Quantity.ToString();
                dgvOrder.Rows[index].Cells[2].Value = item.Total.ToString();
                dgvOrder.Rows[index].Cells[3].Value = item.ItemID.ToString();
            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmMenu frmMenu = new frmMenu();
            this.Hide();
            frmMenu.ShowDialog();
          
        }

        private void frmpayment_Load(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();  

            List<Account> accounts = context.Accounts.ToList();
            List<PaymentMethod> paymentMethods = context.PaymentMethods.ToList();


            cbPayment.DataSource = paymentMethods;
            cbPayment.ValueMember = "PayMethodID";
            cbPayment.DisplayMember = "PayMethodName";

            cbstafforder.DataSource = accounts;
            cbstafforder.ValueMember = "StaffID";
            cbstafforder.DisplayMember = "UserName";
            UpdateTextbox2();



        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(textBox1.Text))
                {
                    textBox1.Text = "";
                    textBox3.Text = "";
                }
                else
                {
                    
                    decimal total = decimal.Parse(textBox1.Text) - decimal.Parse(textBox2.Text);
                     textBox3.Text = total.ToString();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            decimal discountPercentage = numericUpDown1.Value;
            decimal originalTotal = decimal.Parse(textBox2.Text);
            decimal discountAmount = (originalTotal * discountPercentage) / 100;

            // Tính tổng sau giảm giá
            decimal discountedTotal = originalTotal - discountAmount;

            // Cập nhật giá trị vào textBox3
            decimal discount1 = decimal.Parse(textBox1.Text) - discountedTotal;
            textBox3.Text = discount1.ToString();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();

            // Tìm OrderID lớn nhất trong cơ sở dữ liệu
            int maxOrderID = context.Orders.Max(o => o.OrderID);

            // Tạo một biến nextOrderID bằng cách tăng giá trị maxOrderID lên 1
            int nextOrderID = maxOrderID + 1;

            // Tạo một đối tượng Order với OrderID tự động
            Order f = new Order()
            {
                OrderID = nextOrderID, // Gán giá trị nextOrderID cho OrderID
                StaffID = (string)cbstafforder.SelectedValue,
                Note = rBOX.Text,
                PayMethodID = (int)cbPayment.SelectedValue,
                TotalAmount = decimal.Parse(textBox3.Text),
                Date = dateTimePicker2.Value,
            };

            context.Orders.Add(f);
            context.SaveChanges();

            rBOX.Clear();
            textBox3.Clear();
            textBox2.Clear();

            List<OrderDetail> orders = new List<OrderDetail>();

            foreach (DataGridViewRow row in dgvOrder.Rows)
            {
                OrderDetail order1 = new OrderDetail();

                order1.OrderID = nextOrderID; // Gán giá trị nextOrderID cho OrderID của các chi tiết
                order1.ItemID = int.Parse(row.Cells[3].Value.ToString());
                order1.Quantity = int.Parse(row.Cells[1].Value.ToString());
                order1.Total = decimal.Parse(row.Cells[2].Value.ToString());

                orders.Add(order1);
            }

            context.OrderDetails.AddRange(orders);
            context.SaveChanges();
            MessageBox.Show("Payment successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
