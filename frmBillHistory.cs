﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;
using OfficeOpenXml;
using Excel = Microsoft.Office.Interop.Excel;
using iTextSharp.text;

namespace testb1
{
    public partial class frmBillHistory : Form
    {
        public frmBillHistory()
        {
            InitializeComponent();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmMenu m = new frmMenu();
            this.Hide();
            m.ShowDialog();
        }

 
        private void dateStart_ValueChanged(object sender, EventArgs e)
        {

            AccountModel context = new AccountModel();
            var startDate = DateTime.Parse(dateStart.Value.ToString());
            var endDate = DateTime.Parse(DateEnd.Value.ToString());
            var filteredBills = context.Orders
            .Where(b => b.Date >= startDate && b.Date <= endDate)
            .ToList();

            dgvBill.Rows.Clear();
            int indexRow;
            foreach (var item in filteredBills)
            {

                indexRow = dgvBill.Rows.Add();
                dgvBill.Rows[indexRow].Cells[0].Value = item.OrderID;
                dgvBill.Rows[indexRow].Cells[1].Value = item.Date;
                dgvBill.Rows[indexRow].Cells[2].Value = item.Note;
                dgvBill.Rows[indexRow].Cells[3].Value = item.TotalAmount;
                dgvBill.Rows[indexRow].Cells[4].Value = item.PaymentMethod.PayMethodName;
                dgvBill.Rows[indexRow].Cells[5].Value = item.Account.UserName;

            }


        }

        private void DateEnd_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                AccountModel context = new AccountModel();
                var startDate = DateTime.Parse(dateStart.Value.ToString());
                var endDate = DateTime.Parse(DateEnd.Value.ToString());
                var filteredBills = context.Orders
                .Where(b => b.Date >= startDate && b.Date <= endDate)
                .ToList();

                dgvBill.Rows.Clear();
                int indexRow;
                foreach (var item in filteredBills)
                {

                    indexRow = dgvBill.Rows.Add();
                    dgvBill.Rows[indexRow].Cells[0].Value = item.OrderID;
                    dgvBill.Rows[indexRow].Cells[1].Value = item.Date;
                    dgvBill.Rows[indexRow].Cells[2].Value = item.Note;
                    dgvBill.Rows[indexRow].Cells[3].Value = item.TotalAmount;
                    dgvBill.Rows[indexRow].Cells[4].Value = item.PaymentMethod.PayMethodName;
                    dgvBill.Rows[indexRow].Cells[5].Value = item.Account.UserName;

                }
            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void fillDgvBill(List<Order> listOrders)
        {
            dgvBill.Rows.Clear();
            int indexRow;
            foreach (var item in listOrders)
            {
                indexRow = dgvBill.Rows.Add();
                dgvBill.Rows[indexRow].Cells[0].Value = item.OrderID;
                dgvBill.Rows[indexRow].Cells[1].Value = item.Date;
                dgvBill.Rows[indexRow].Cells[2].Value = item.Note;
                dgvBill.Rows[indexRow].Cells[3].Value = item.TotalAmount;
                dgvBill.Rows[indexRow].Cells[4].Value = item.PaymentMethod.PayMethodName;
                dgvBill.Rows[indexRow].Cells[5].Value = item.Account.UserName;

            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            List<Order> listOrders = context.Orders.ToList();
            string findID = txtFind.Text;
            testb1.context.Order orderFind = context.Orders.FirstOrDefault(p => p.OrderID.ToString() == findID);
            try
            {
                if (txtFind.Text != "")
                {
                    try
                    {
                        if (orderFind == null)
                        {
                            dgvBill.Rows.Clear();
                        }
                        else if (findID == orderFind.OrderID.ToString())
                        {
                            dgvBill.Rows.Clear();
                            int indexRow;
                            indexRow = dgvBill.Rows.Add();
                            dgvBill.Rows[indexRow].Cells[0].Value = orderFind.OrderID;
                            dgvBill.Rows[indexRow].Cells[1].Value = orderFind.Date;
                            dgvBill.Rows[indexRow].Cells[2].Value = orderFind.Note;
                            dgvBill.Rows[indexRow].Cells[3].Value = orderFind.TotalAmount;
                            dgvBill.Rows[indexRow].Cells[4].Value = orderFind.PaymentMethod.PayMethodName;
                            dgvBill.Rows[indexRow].Cells[5].Value = orderFind.Account.UserName;
                        }

                        else if (txtFind.Text == "")
                        {
                            fillDgvBill(listOrders);
                        }
                    }
                    catch
                    {

                    }
                }
                else if (txtFind.Text == "")
                {

                    fillDgvBill(listOrders);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ExportFile(string path)
        {
            Excel.Application application = new Excel.Application();
            application.Application.Workbooks.Add(Type.Missing);
            for (int i = 0; i < dgvBill.Columns.Count; i++)
            {
                application.Cells[1, i + 1] = dgvBill.Columns[i].HeaderText;
            }
            for (int i = 0; i < dgvBill.Rows.Count; i++)
            {
                for (int j = 0; j < dgvBill.Columns.Count; j++)
                {
                    application.Cells[i + 2, j + 1] = dgvBill.Rows[i].Cells[j].Value;
                }
            }
            application.Columns.AutoFit();
            application.ActiveWorkbook.SaveCopyAs(path);
            application.ActiveWorkbook.Saved = true;
        }
        private void BillHistory_Load(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            List<Order> listOrders = context.Orders.ToList();


            fillDgvBill(listOrders);

        }

   

        private void dgvBill_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            AccountModel context = new AccountModel();
            try
            {
                List<Order> listOrder = context.Orders.ToList();
                DataGridViewRow row = dgvBill.Rows[e.RowIndex];
                txtUserID.Text = row.Cells[5].Value.ToString();
                rbNote.Text = row.Cells[2].Value.ToString();
                txtPayment.Text = row.Cells[4].Value.ToString();
                txtTotal.Text = row.Cells[3].Value.ToString();

                int findID = int.Parse(row.Cells[0].Value.ToString());

                // Sử dụng ToList() để chuyển kết quả thành danh sách
                List<OrderDetail> orderDetails = context.OrderDetails.Where(p => p.OrderID == findID).ToList();

                if (orderDetails.Any())
                {
                    dgvOrderDetail.Rows.Clear();

                    foreach (var orderDetail in orderDetails)
                    {
                        int indexRow = dgvOrderDetail.Rows.Add();
                        dgvOrderDetail.Rows[indexRow].Cells[0].Value = orderDetail.Item.ItemName;
                        dgvOrderDetail.Rows[indexRow].Cells[1].Value = orderDetail.Quantity;
                        dgvOrderDetail.Rows[indexRow].Cells[2].Value = orderDetail.Item.Price;
                        dgvOrderDetail.Rows[indexRow].Cells[3].Value = orderDetail.Total;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message );
            }
        }

        private void btnExportFile_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Export Excel";
            saveFileDialog.Filter = "Excel (*.xlsx)|*.xlsx | Excel 2003 (*.xls)|*.xls";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ExportFile(saveFileDialog.FileName);
                    MessageBox.Show("File exported successfully!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to export file!\n" + ex.Message);
                }
            }
        }
    }
}
