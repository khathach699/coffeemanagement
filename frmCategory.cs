﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;

namespace testb1
{
    public partial class frmCategory : Form
    {
        public frmCategory()
        {
            InitializeComponent();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmMenu m = new frmMenu();
            this.Hide();
            m.ShowDialog();
        }

        private void frmCategory_Load(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            List<Category> listCate = context.Categories.ToList();

            fillDgvCate(listCate);


        }

        private void fillDgvCate(List<Category> listCate)
        {
            dgvCategory.Rows.Clear();
            int indexRow;
            foreach (var item in listCate)
            {
                indexRow = dgvCategory.Rows.Add();
                dgvCategory.Rows[indexRow].Cells[0].Value = item.CategoryID;
                dgvCategory.Rows[indexRow].Cells[1].Value = item.CategoryName;
            }

        }

        private void btnCategoryAdd_Click(object sender, EventArgs e)
        {
            Category cate = new Category();
            AccountModel context = new AccountModel();
            try
            {
                if (txtCategoryName.Text == "" || txtCategoryID.Text == "")
                {
                    throw new Exception("Vui long nhap day du thong tin");
                }
                else
                {

                    DataGridViewRow row = dgvCategory.Rows.Cast<DataGridViewRow>().FirstOrDefault(p => p.Cells[0].Value + "" == txtCategoryID.Text + "");
                    if (row == null)
                    {
                        int newrow = dgvCategory.Rows.Add();
                        dgvCategory.Rows[newrow].Cells[0].Value = txtCategoryID.Text;
                        dgvCategory.Rows[newrow].Cells[1].Value = txtCategoryName.Text;
                        cate.CategoryID = int.Parse(txtCategoryID.Text);
                        cate.CategoryName = txtCategoryName.Text;
                        MessageBox.Show("Đã thêm thành công");
                    }
                    txtCategoryID.Text = "";
                    txtCategoryName.Text = "";
                    context.Categories.Add(cate);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private int GetSelectedRow(string CategoryID)
        {
            for (int i = 0; i < dgvCategory.Rows.Count; i++)
            {
                if (dgvCategory.Rows[i].Cells[0].Value != null && dgvCategory.Rows[i].Cells[0].Value.ToString() == CategoryID)
                {
                    return i;
                }
            }
            return -1;
        }



        private void InsertUpdate(int selectedRow)
        {
            try
            {
                int category = int.Parse(txtCategoryID.Text);
                AccountModel context = new AccountModel();
                Category find = context.Categories.FirstOrDefault(p => p.CategoryID == category);
                if (find != null)
                {
                    int updateID = int.Parse(txtCategoryID.Text);
                    Category cateUp = context.Categories.FirstOrDefault(p => p.CategoryID == updateID);
                    dgvCategory.Rows[selectedRow].Cells[0].Value = txtCategoryID.Text;
                    dgvCategory.Rows[selectedRow].Cells[1].Value = txtCategoryName.Text;

                    cateUp.CategoryID = int.Parse(txtCategoryID.Text);
                    cateUp.CategoryName = txtCategoryName.Text;
                    context.SaveChanges();
                }
                else
                {
                    MessageBox.Show("Not found ID, please enter id again", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void dgvCategory_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            AccountModel context = new AccountModel();
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row = dgvCategory.Rows[e.RowIndex];
                    txtCategoryID.Text = row.Cells[0].Value.ToString();
                    txtCategoryName.Text = row.Cells[1].Value.ToString();
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi");
            }

        }

        private void btnCategoryDetele_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            Category cateDlt = context.Categories.FirstOrDefault(p => p.CategoryID.ToString() == txtCategoryID.Text);
            try
            {
                int selectedRow = GetSelectedRow(txtCategoryID.Text);
                if (selectedRow == -1)
                {
                    MessageBox.Show("Không tìm thấy ID, vui lòng nhập ID hợp lệ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DialogResult dr = MessageBox.Show("Bạn có muốn xóa?", "YES/NO", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        cateDlt.CategoryID = int.Parse(txtCategoryID.Text);
                        cateDlt.CategoryName = txtCategoryName.Text;
                        dgvCategory.Rows.RemoveAt(selectedRow);
                        context.Categories.Remove(cateDlt);
                        MessageBox.Show("Xóa thành công!", "Thông Báo", MessageBoxButtons.OK);
                        context.SaveChanges();
                        txtCategoryID.Text = "";
                        txtCategoryName.Text = "";
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Xóa thành công!", "Thông Báo", MessageBoxButtons.OK);
            }


        }

        private void btnCategoryUpdate_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            int updateID = int.Parse(txtCategoryID.Text);
            Category cateUp = context.Categories.FirstOrDefault(p => p.CategoryID == updateID);

            try
            {
                if (txtCategoryName.Text == "")
                {
                    throw new Exception("Vui lòng nhập đầy đủ thông tin");
                }

                int selectedRow = GetSelectedRow(txtCategoryID.Text);

                if (selectedRow == -1)
                {
                    MessageBox.Show("Không tìm thấy ID, vui lòng nhập ID hợp lệ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    InsertUpdate(selectedRow);
                    MessageBox.Show("Cập nhật dữ liệu thành công!", "Thông Báo", MessageBoxButtons.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

