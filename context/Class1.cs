﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testb1
{
    public static class SharedData
    {
        public static string LoggedInUsername { get; set; }
        public static int UserRole { get; set; }
        public static string CurrentUsername { get; set; }
        public static decimal SharedTotal { get; set; }
        public static DataGridView SharedDataGridView { get; set; }
        public class ShiftWork
        {
            public int ShiftID { get; set; }
            public string ShiftName { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }

            
        }

    }


}
