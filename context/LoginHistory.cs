namespace testb1.context
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LoginHistory")]
    public partial class LoginHistory
    {
        public int LoginHistoryID { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [StringLength(50)]
        public string StaffID { get; set; }

        public int? ShiftID { get; set; }

        public int? PositionID { get; set; }

        public virtual Position Position { get; set; }

        public virtual ShiftWork ShiftWork { get; set; }
    }
}
