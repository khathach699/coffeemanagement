﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;

namespace testb1
{

    public partial class frmMenu : Form
    {


        public static int ItemCount = 0;
        public static decimal TotalAmount = 0;
        public static List<OrderDetail> OrderItems = new List<OrderDetail>();



        private void UpdateTotal()
        {
            decimal total = dgvOrder.Rows.Cast<DataGridViewRow>().Sum(row => decimal.Parse(row.Cells[3].Value.ToString()));
            txtTotal.Text = total.ToString();

            // Cập nhật giá trị SharedTotal
            SharedData.SharedTotal = total;

            // Cập nhật số lượng sản phẩm
            ItemCount = dgvOrder.Rows.Count;

            // Cập nhật danh sách sản phẩm
            OrderItems.Clear();
            foreach (DataGridViewRow row in dgvOrder.Rows)
            {
                OrderDetail item = new OrderDetail();
                //item.Item.ItemName = row.Cells[0].Value.ToString();
                item.Quantity = int.Parse(row.Cells[1].Value.ToString());
                item.Total = decimal.Parse(row.Cells[3].Value.ToString());
                item.ItemID = int.Parse(row.Cells[4].Value.ToString());
                //item.Item.ItemName = row.Cells[0].Value.ToString();
                OrderItems.Add(item);
            }
        }

        public frmMenu()
        {
            InitializeComponent();
        }


        private void button10_Click(object sender, EventArgs e)
        {
            frmpayment frmpayment = new frmpayment();
            this.Hide();
            frmpayment.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBillHistory invoiceHistory = new frmBillHistory();
            this.Hide();
            invoiceHistory.ShowDialog();
        }

        private void manToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void accountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAccout f = new frmAccout();
            this.Hide();
            f.ShowDialog();
        }

        private void verificationCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmHistoryLogin h = new frmHistoryLogin();
            this.Hide();
            h.ShowDialog();
        }

        private void billToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCategory c = new frmCategory();
            this.Hide();
            c.ShowDialog();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmItem m = new frmItem();
            this.Hide();
            m.ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPosition p = new frmPosition();
            this.Hide();
            p.ShowDialog();
        }

        private void shiftWorkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmWorkShift shiftManager = new frmWorkShift();
            this.Hide();
            shiftManager.ShowDialog();
        }


        //private void UpdateTotal()
        //{
        //    decimal total = dgvOrder.Rows.Cast<DataGridViewRow>().Sum(row => decimal.Parse(row.Cells[3].Value.ToString()));
        //    txtTotal.Text = total.ToString();

        //    // Cập nhật giá trị SharedTotal
        //    SharedData.SharedTotal = total;
        //}
        private void Menu_Load(object sender, EventArgs e)
        {

            AccountModel context = new AccountModel();
            List<Category> categories = context.Categories.ToList();

            cbCategory.DataSource = categories;
            cbCategory.DisplayMember = "CategoryName";
            cbCategory.ValueMember = "CategoryID";

            if ((SharedData.UserRole == 1))
            {
                label4.Text = "Welcome Manager";

            }
            else
            {
                label4.Text = "Welcome Staff";

            }

            }
       

        private void ShowItemsForCategory(int categoryID)
        {
            AccountModel context = new AccountModel();
            List<Item> items = context.Items.Where(item => item.CategoryID == categoryID).ToList();

            dataGridView1.Rows.Clear();
            foreach (Item item in items)
            {
                int newRow = dataGridView1.Rows.Add(item.ItemName, item.Price, item.ItemID);
            }
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            List<OrderDetail> orders = new List<OrderDetail>();

            // Duyệt qua danh sách các dòng trong dgvOrder và thêm các đối tượng Order1 tương ứng vào danh sách orders

            foreach (DataGridViewRow row in dgvOrder.Rows)
            {
                OrderDetail order1 = new OrderDetail();
                //order1.OrderID = i+1;
                
                order1.OrderID = 1;
                order1.ItemID = int.Parse(row.Cells[4].Value.ToString());
                order1.Item.ItemName = row.Cells[0].Value.ToString();
                order1.Quantity = int.Parse(row.Cells[1].Value.ToString());
                order1.Item.Price = decimal.Parse(row.Cells[2].ToString());
                order1.Total = decimal.Parse(row.Cells[3].Value.ToString());
                //order1.Table = 1;
                orders.Add(order1);
            }

            // Lưu các đối tượng Order1 vào CSDL
            AccountModel context = new AccountModel();
            context.OrderDetails.AddRange(orders);
            context.SaveChanges();
            MessageBox.Show("Add successful");
        }

        private void btnPayment_Click_1(object sender, EventArgs e)
        {



            frmpayment frmpayment = new frmpayment();
            this.Hide();
            frmpayment.ShowDialog();

        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex < 0)
                return;

            string name = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            decimal price = (decimal)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            int id = (int)dataGridView1.Rows[e.RowIndex].Cells[2].Value;

            DataGridViewRow existingRow = dgvOrder.Rows
                .Cast<DataGridViewRow>()
                .FirstOrDefault(row => row.Cells[4].Value.ToString() == id.ToString());

            if (existingRow != null)
            {
                int quantity = (int)existingRow.Cells[1].Value + (int)numericUpDown1.Value;
                existingRow.Cells[1].Value = quantity;
                numericUpDown1.Value = 1;
                existingRow.Cells[3].Value = quantity * price;
            }
            else
            {
                int newRow = dgvOrder.Rows.Add(name, 1, price, price, id);
            }

            UpdateTotal();
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Category selectedCategory = (Category)cbCategory.SelectedItem;

            if (selectedCategory != null)
            {
                int selectedCategoryID = selectedCategory.CategoryID;
                ShowItemsForCategory(selectedCategoryID);
            }
        }

        private void dgvOrder_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            int row = e.RowIndex;
            int quantity = int.Parse(dgvOrder.Rows[row].Cells[1].Value.ToString());

            if (quantity > 0)
            {
                if (numericUpDown1.Value <= quantity || quantity == 1)
                    dgvOrder.Rows[row].Cells[1].Value = quantity - numericUpDown1.Value;
                else
                    MessageBox.Show("The quantity you want to delete is greater than the existing quantity!!!");
                numericUpDown1.Value = 1;
                dgvOrder.Rows[row].Cells[3].Value = decimal.Parse(dgvOrder.Rows[row].Cells[1].Value.ToString()) * decimal.Parse(dgvOrder.Rows[row].Cells[2].Value.ToString());
                UpdateTotal();
            }
            else
            {
                dgvOrder.Rows.RemoveAt(row);
                UpdateTotal();
            }
        }
    }
}


