﻿using DevExpress.Data.Svg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;

namespace testb1
{
    public partial class frmItem : Form
    {
        public frmItem()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            frmMenu m = new frmMenu();
            this.Hide();
            m.ShowDialog();
        }

        private void Dish_Load(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();  
            List<Category> listCate = context.Categories.ToList();
            List<Item> listItems = context.Items.ToList();

            fillCbbCate(listCate);
            fillDgvMenu(listItems);

        }



        private void fillCbbCate(List<Category> listCate)
        {
            cbCategory.DataSource = listCate;

            cbCategory.DisplayMember = "CategoryName";

            cbCategory.ValueMember = "CategoryID";
        }

        private void fillDgvMenu(List<Item> listItems)
        {
            dgvItem.Rows.Clear();
            int indexRow;
            foreach (var item in listItems)
            {
                indexRow = dgvItem.Rows.Add();
                dgvItem.Rows[indexRow].Cells[0].Value = item.ItemID;
                dgvItem.Rows[indexRow].Cells[1].Value = item.ItemName;
                dgvItem.Rows[indexRow].Cells[2].Value = item.Category.CategoryName;
                dgvItem.Rows[indexRow].Cells[3].Value = item.Price;
            }
        }


        private int GetSelectedRow(string ItemID)
        {
            for (int i = 0; i < dgvItem.Rows.Count; i++)
            {
                if (dgvItem.Rows[i].Cells[0].Value != null && dgvItem.Rows[i].Cells[0].Value.ToString() == ItemID)
                {
                    return i;
                }
            }
            return -1;
        }

        private void btnAccountAdd_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();

            Item newitem = new Item();
            string item = txtItem.Text;
            int price = int.Parse(txtPrice.Text);
            try
            {
                foreach (DataGridViewRow row in dgvItem.Rows)
                {
                    if (row.Cells[0].Value != null && row.Cells[0].Value.ToString() == item)
                    {
                        MessageBox.Show("Item already exists, please check again.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                if (string.IsNullOrEmpty(txtItem.Text) || string.IsNullOrEmpty(txtItemName.Text) || string.IsNullOrEmpty(txtPrice.Text))
                {
                    throw new Exception("Please enter all required information.");
                }
                else if (price <= 0)
                {
                    MessageBox.Show("Price must be greater than or equal to 0, please enter again.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DataGridViewRow row = dgvItem.Rows.Cast<DataGridViewRow>().FirstOrDefault(p => (p.Cells[0].Value + "") == (txtItem.Text + ""));
                    if (row == null)
                    {
                        int newrow = dgvItem.Rows.Add();
                        dgvItem.Rows[newrow].Cells[0].Value = item;
                        dgvItem.Rows[newrow].Cells[1].Value = txtItemName.Text;
                        dgvItem.Rows[newrow].Cells[2].Value = cbCategory.Text;
                        dgvItem.Rows[newrow].Cells[3].Value = float.Parse(txtPrice.Text).ToString();
                        newitem.ItemID = int.Parse(txtItem.Text);
                        newitem.ItemName = txtItemName.Text;
                        newitem.CategoryID = (int)cbCategory.SelectedValue;
                        newitem.Price = decimal.Parse(txtPrice.Text);

                        MessageBox.Show("Added successfully.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        context.Items.Add(newitem);
                        context.SaveChanges();
                        txtItem.Text = "";
                        txtItemName.Text = "";
                        cbCategory.Text = "";
                        txtPrice.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InsertUpdate(int selectedRow)
        {
            AccountModel context = new AccountModel();
            int updateID = int.Parse(txtItem.Text);
            Item itemUp = context.Items.FirstOrDefault(p => p.ItemID == updateID);
            dgvItem.Rows[selectedRow].Cells[0].Value = txtItem.Text;
            dgvItem.Rows[selectedRow].Cells[1].Value = txtItemName.Text;
            dgvItem.Rows[selectedRow].Cells[2].Value = cbCategory.Text;
            dgvItem.Rows[selectedRow].Cells[3].Value = txtPrice.Text;

            itemUp.ItemID = int.Parse(txtItem.Text);
            itemUp.ItemName = txtItemName.Text;
            itemUp.CategoryID = (int)cbCategory.SelectedValue;
            itemUp.Price = decimal.Parse(txtPrice.Text);
            context.SaveChanges();
        }


        private void btnAccountUpdate_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            int updateID = int.Parse(txtItem.Text);
            Item itemUp = context.Items.FirstOrDefault(p => p.ItemID == updateID);

            try
            {
                if (string.IsNullOrEmpty(txtItem.Text) || string.IsNullOrEmpty(txtItemName.Text) || string.IsNullOrEmpty(txtPrice.Text))
                {
                    throw new Exception("Please enter all required information.");
                }

                if (!float.TryParse(txtPrice.Text, out float price) || price <= 0)
                {
                    throw new Exception("Price must be a valid number greater than 0.");
                }

                int selectedRow = GetSelectedRow(txtItem.Text);
                if (selectedRow == -1)
                {
                    MessageBox.Show("Item not found for updating. Please enter a valid Item ID.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    InsertUpdate(selectedRow);
                    MessageBox.Show("Data updated successfully.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnAccountDetele_Click(object sender, EventArgs e)
        {
            AccountModel context = new AccountModel();
            Item itemDlt = context.Items.FirstOrDefault(p => p.ItemID.ToString() == txtItem.Text);

            try
            {
                if (string.IsNullOrEmpty(txtItem.Text) || txtItemName.Text == "" || txtPrice.Text == "")
                {
                    throw new Exception("Please enter all required information.");
                }

                int selectedRow = GetSelectedRow(txtItem.Text);
                if (selectedRow == -1)
                {
                    MessageBox.Show("Item not found for deletion.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult dr = MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        itemDlt.ItemID = int.Parse(txtItem.Text);
                        itemDlt.ItemName = txtItemName.Text;
                        itemDlt.CategoryID = (int)cbCategory.SelectedValue;
                        itemDlt.Price = decimal.Parse(txtPrice.Text);
                        dgvItem.Rows.RemoveAt(selectedRow);
                        context.Items.Remove(itemDlt);
                        MessageBox.Show("Deleted successfully.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        context.SaveChanges();
                        txtItem.Text = "";
                        txtItemName.Text = "";
                        cbCategory.Text = "";
                        txtPrice.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvItem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            AccountModel context = new AccountModel();
            try
            {
               if(dgvItem.Rows.Count >= 0) {
                    DataGridViewRow row = new DataGridViewRow();
                    row = dgvItem.Rows[e.RowIndex];
                    txtItem.Text = row.Cells[0].Value.ToString();
                    txtItemName.Text = row.Cells[1].Value.ToString();
                    cbCategory.Text = row.Cells[2].Value.ToString();
                    txtPrice.Text = row.Cells[3].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string key = txtFind.Text;
            AccountModel context = new AccountModel();
            if (string.IsNullOrEmpty(key))
            {
                fillDgvMenu(context.Items.ToList());
            }
            else
            {
                var search = context.Items.Where(a => a.ItemID.ToString().Contains(key) ||
                                                        a.ItemName.Contains(key) ||
                                                        a.Category.CategoryName.ToString().Contains(key) ||
                                                        a.Price.ToString().Contains(key))
                                                        .ToList();

                fillDgvMenu(search);
            }

        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar)){
                e.Handled = true;
            }
        }
    }
}
