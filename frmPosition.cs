﻿
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Security.Principal;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using testb1.context;
    using static testb1.SharedData.ShiftWork;


    namespace testb1
    {
        public partial class frmPosition : Form
        {
            public frmPosition()
            {
                InitializeComponent();

            }
            private void PositionAccount_Load(object sender, EventArgs e)
            {
                    AccountModel context = new AccountModel();
                    List<ShiftWork> shiftWorks = context.ShiftWorks.ToList();

                    // Tạo một danh sách chuỗi để lưu thông tin ShiftName và thời gian bắt đầu và kết thúc
                    List<string> shiftInfoList = new List<string>();
                    foreach (var shift in shiftWorks)
                    {
                        string shiftInfo = $"{shift.ShiftName} ({shift.StartTime.ToString("HH:mm")} - {shift.EndTime.ToString("HH:mm")})";
                        shiftInfoList.Add(shiftInfo);
                    }
                    shiftInfoList.Insert(0, "");
                    comboBox1.DataSource = shiftInfoList;
                    textBox1.Text = SharedData.CurrentUsername;
        }

            private void btnLogin_Click_1(object sender, EventArgs e)
            {
                //try
                //{
                    if ((SharedData.UserRole == 1 && radioButton1.Checked) || (SharedData.UserRole == 2 && radioButton2.Checked))
                    {
                        // Determine the selected PositionID based on the checked radio button
                        int positionID;
                        if (radioButton1.Checked)
                        {
                            positionID = 1; // You can set the appropriate PositionID value
                        }
                        else
                        {
                            positionID = 2; // You can set the appropriate PositionID value
                          }

                LoginHistory h = new LoginHistory()
                {
                            Date = dateTimePicker1.Value,
                            ShiftID = comboBox1.SelectedIndex, // Assuming this is correct
                            PositionID = positionID, // Set the PositionID based on the radio button
                            StaffID = textBox1.Text
                        };

                        AccountModel context = new AccountModel();
                        context.LoginHistories.Add(h);
                        context.SaveChanges();
                        frmMenu f = new frmMenu();
                        this.Hide();
                        f.ShowDialog();
                        MessageBox.Show("Login Successfully", "notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Position is not correct, please double-check", "notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show("Plese choose shift");
                //}
            }


            private void btnExit_Click(object sender, EventArgs e)
            {
                frmLogin l = new frmLogin();
                this.Hide();
                l.ShowDialog();
            }

     
        }
    }
