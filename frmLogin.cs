﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using testb1.context;
using static testb1.SharedData;

namespace testb1
{
    public partial class frmLogin : Form
    {

        public string TxtLoginValue
        {
            get { return txtUserName.Text; }
            set { txtUserName.Text = value; }
        }

        public frmLogin()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            // Change the value of TxtLoginValue based on txtUserName when the form is loaded
            TxtLoginValue = Properties.Settings.Default.username;

            txtPassWork.Text = Properties.Settings.Default.password;

            if (Properties.Settings.Default.username != "")
            {
                ckSaveLogin.Checked = true;
            }
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string userName = txtUserName.Text;
                string password = txtPassWork.Text;
                if (userName.Trim() == "") { MessageBox.Show("Please enter a username."); }
                else if (password.Trim() == "") { MessageBox.Show("Please enter a password."); }
                else
                {
                    AccountModel context = new AccountModel();
                    Account account = context.Accounts.FirstOrDefault(p => (p.UserName == userName) && p.PassWord == password);

                    if (account != null)
                    {
                        // Get the user's role from the database
                        SharedData.CurrentUsername = account.UserName;
                        SharedData.UserRole = account.PositionID.Value;
                        MessageBox.Show("Login successful", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmPosition p = new frmPosition();
                        this.Hide();
                        p.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Invalid username or password", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void ckSaveLogin_CheckedChanged(object sender, EventArgs e)
        {

            if (txtUserName.Text != "" && txtPassWork.Text != "")
            {
                if (ckSaveLogin.Checked)
                {
                    string user = txtUserName.Text;
                    string password = txtPassWork.Text;
                    Properties.Settings.Default.username = user;
                    Properties.Settings.Default.password = password;

                    Properties.Settings.Default.Save();
                }
                else
                {
                    Properties.Settings.Default.Reset();
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmForgot forgot = new frmForgot();
            this.Hide();
            forgot.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {

            if (txtPassWork.PasswordChar == '*')
            {
                button4.BringToFront();
                txtPassWork.PasswordChar = '\0';
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtPassWork.PasswordChar == '\0')
            {
                button5.BringToFront();
                txtPassWork.PasswordChar = '*';
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            TxtLoginValue = txtUserName.Text;
        }
    }
}
