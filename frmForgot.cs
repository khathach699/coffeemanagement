﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using testb1.context;

namespace testb1
{
    public partial class frmForgot : Form
    {
        public frmForgot()
        {
            InitializeComponent();
        }

        Random random = new Random();
        string otps; // Change data type to string for OTP

        
        

        private void button2_Click_1(object sender, EventArgs e)
        {
            frmLogin l = new frmLogin();
            this.Hide();
            l.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                otps = random.Next(100000, 1000000).ToString(); // Generate and convert to string
                var fromaddress = new MailAddress("khathach699@gmail.com");
                var toaddress = new MailAddress(textBox1.Text); // Get the text from the textBox
                const string frompass = "rqdihefbfiuyahmz";
                const string subject = "OTP CODE";
                string body = otps;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromaddress.Address, frompass),
                    Timeout = 200000
                };
                using (var message = new MailMessage(fromaddress, toaddress))
                {
                    message.Subject = subject;
                    message.Body = "MY OTP: " + body;
                    smtp.Send(message);
                }
                MessageBox.Show("OTP send Email");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnXacNhan_Click_1(object sender, EventArgs e)
        {
            try
            {
                string enteredEmail = textBox1.Text;
                if (otps.Equals(textBox4.Text) || string.IsNullOrEmpty(textBox4.Text)) // Compare OTP strings
                {
                    using (AccountModel context = new AccountModel())
                    {
                        // Attempt to retrieve the account based on the entered email
                        Account account = context.Accounts.FirstOrDefault(a => a.Email == enteredEmail);
                        if (account != null)
                        {
                            MessageBox.Show("Verification successful");
                            MessageBox.Show("Your PassWord: " + account.PassWord, "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Email does not exist in the system", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("EIncorrect OTP", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

    

        private void forgot_Load(object sender, EventArgs e)
        {

        }
    }
}
